import {isSignedIn} from "./src/components/navigator/Auth";
import React, {Component} from 'react';
import {AppRegistry} from "react-native";
import {createRootNavigator} from "./src/components/navigator/Router";

export class App extends React.Component {

    constructor(props) {
        super(props);

        this.state = {
            signedIn: false,
            checkedSignIn: false
        };
    }

    componentWillMount() {
        isSignedIn()
            .then(res => this.setState({signedIn: res, checkedSignIn: true}))
            .catch(err => alert("An error occurred"));
    }
    render() {
        const { checkedSignIn, signedIn } = this.state;

        if (!checkedSignIn) {
            return null;
        }

        const Layout = createRootNavigator(signedIn);
        return <Layout />;
    }
}

AppRegistry.registerComponent("TestTaskProject", () => App);


