import {
    StyleSheet,
     TouchableOpacity
} from 'react-native';
import React from 'react';
import FontAwesome from "react-native-vector-icons/FontAwesome";
import ListAnswers from "./ListAnswers";
import {StackNavigator} from 'react-navigation';

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
});

const StackOverflow = () => {
    stackNav;
    return (
        <ListAnswers {...this.props}/>
    );
};


const stackNav = StackNavigator({
    StackOverFlow: {
        screen: StackOverflow,
        navigationOptions: ({navigation}) => ({
            title: 'StackOverflow',
            headerLeft: (
                <TouchableOpacity onPress={() => navigation.navigate("DrawerOpen")}>
                    <FontAwesome name="bars" size={30}/>
                </TouchableOpacity>

            ),
            headerStyle: {paddingRight: 10, paddingLeft: 10}
        })
    }
});


export default stackNav;