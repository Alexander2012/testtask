import {Text, StyleSheet, View, TouchableOpacity} from "react-native";
import React, {Component} from "react";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import * as utils from "util";
import {StackNavigator} from "react-navigation";
import {onGetName, USER_NAME} from "../navigator/Auth";
import {AsyncStorage} from "react-native";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fafff8',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
});

class FirstScreen extends Component {
    constructor(props) {
        super(props);
        AsyncStorage.getItem(USER_NAME).then((value) => {
            this.setState({name : value})
        }).done();
    }


    state = {
        name: "UserName"
    };

    render() {
        const {state} = this.props.navigation;
        var nameUser = state.params ? state.params.name : null;


        if (nameUser == null) {
            nameUser = this.state.name
        }

        return <View style={styles.container}>
            <Text style={styles.welcome}>
                Hello, {nameUser}!
            </Text>
        </View>
    }
}


const stackNav = StackNavigator({

    SignedIn: {
        screen: FirstScreen,
        navigationOptions: ({navigation}) => ({

            title: "HOME",
            headerLeft: (
                <TouchableOpacity onPress={() => navigation.navigate("DrawerOpen")}>
                    <FontAwesome name="bars" size={30}/>
                </TouchableOpacity>

            ),
            headerStyle: {paddingRight: 10, paddingLeft: 10}
        })
    }
});


export default stackNav;