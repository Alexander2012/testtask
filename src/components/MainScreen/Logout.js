import {
    Button,
    StyleSheet,
    TouchableOpacity,
    View
} from 'react-native';
import React, {Component} from "react";
import FontAwesome from "react-native-vector-icons/FontAwesome";
import {StackNavigator} from 'react-navigation';
import {onRemoveName, onSignOut} from "../navigator/Auth";

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#F5FCFF',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },

    loginButton: {
        paddingVertical: 15,
        backgroundColor: '#2980b6',
        textAlign: 'center',
        color: '#fff'
    }

});

function onLogout() {
    onRemoveName();
    onSignOut().then(() => this.props.navigation.navigate("SignedOut"));
}


// create a component
class Logout extends Component {
    constructor(props) {
        super(props);
        onLogout = onLogout.bind(this);
    }

    render() {
        return (
            <View style={styles.container}>
                <Button onLogOut onPress={onLogout} title='LogOut' style={styles.loginButton}/>
            </View>
        );
    }
}




const logout = StackNavigator({

    Logout: {
        screen: Logout,
        navigationOptions: ({navigation}) => ({
            title: "LogOut",
            headerLeft: (
                <TouchableOpacity onPress={() => navigation.navigate("DrawerOpen")}>
                    <FontAwesome name="bars" size={30}/>
                </TouchableOpacity>

            ),
            headerStyle: {paddingRight: 10, paddingLeft: 10}
        })
    }
});



export default logout;
