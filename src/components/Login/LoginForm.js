import React, {Component} from 'react';
import {
    View,
    Alert,
    Text,
    TextInput,
    Form,
    TextValidator,
    TouchableOpacity,
    Image,
    StyleSheet,
    StatusBar
} from 'react-native';
import {onSignIn, onSaveName} from "../navigator/Auth";
import SignedIn from "../MainScreen/MenuScreen";
import {NavigationActions} from 'react-navigation'

class LoginForm extends Component {
    constructor(props) {
        super(props);
        this.state = {};
        this.onSubmit = this.onSubmit.bind(this);
    }

    state = {
        username: null,
        password: null
    };


    onSubmit() {
        const {username, password} = this.state;
        // let reg = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
        if (username != null && password != null && Object.keys(password).length > 4) {
            const navigationAction = NavigationActions.navigate({
                routeName: 'SignedIn',
                action: NavigationActions.navigate({
                    routeName: 'SignedIn',
                    params: {name: username}
                })
            });
            onSaveName(username);
            onSignIn().then(() => this.props.navigation.dispatch(navigationAction));
        }
        else {
            Alert.alert('Please input email and password. Password need to be more 4 characters.');
        }
    }


    render() {
        return (
            <View style={styles.container}>

                <View style={styles.SectionStyle}>

                    <StatusBar barStyle="light-content"/>
                    <Image source={require('../images/person.png')} style={styles.imageStyle}/>
                    <TextInput style={styles.input}
                               autoCapitalize="none"
                               onChangeText={value => this.setState({username: value})}

                               autoCorrect={false}
                               keyboardType='email-address'
                               returnKeyType="next"

                               placeholder='Login'
                               placeholderTextColor='#000000'/>
                </View>
                <View style={styles.SectionStyle}>
                    <StatusBar barStyle="light-content"/>
                    <Image source={require('../images/locker.png')} style={styles.imageStyle}/>
                    <TextInput style={styles.input}
                        //                                                       returnKeyType="go" ref={(input)=> this.passwordInput = input}
                               placeholder='Password'
                               secureTextEntry={true}

                               onChangeText={value => this.setState({password: value})}
                               placeholderTextColor='#000000'
                               secureTextEntry/>

                </View>
                <TouchableOpacity style={styles.buttonContainer}
                                  onPress={this.onSubmit}>
                    <Text style={styles.buttonText}>LOGIN</Text>
                </TouchableOpacity>
            </View>
        );
    }


}

const styles = StyleSheet.create({
    container: {
        padding: 20
    },
    input: {
        flex: 1,
        height: 40,
        color: '#000000'
    },
    buttonContainer: {
        backgroundColor: '#78db47',
        paddingVertical: 15
    },
    buttonText: {
        color: '#fff',
        textAlign: 'center',
        fontWeight: '700'
    },

    SectionStyle: {
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'stretch',
        backgroundColor: '#fff',
        borderWidth: .5,
        borderColor: '#000',
        height: 40,
        borderRadius: 5,
        margin: 10
    },

    titleText: {
        fontSize: 40,
        fontWeight: 'bold',
        justifyContent: 'center',
        alignItems: 'center'
    },

    imageStyle: {
        padding: 10,
        margin: 5,
        height: 25,
        width: 25,
        resizeMode: 'stretch',
        alignItems: 'center'
    },
    loginButton: {
        backgroundColor: '#2980b6',
        color: '#fff'
    }

});

export default LoginForm;
