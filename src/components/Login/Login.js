import React, {Component} from 'react';
import {View, Text, StyleSheet, KeyboardAvoidingView} from 'react-native';
import LoginForm from './LoginForm';

class Login extends Component {

    render() {
        return (
            <KeyboardAvoidingView behavior="padding" style={styles.container}>
                <View style={{flex: 1, justifyContent: "center", alignItems: "center"}}>
                    <Text
                        adjustsFontSizeToFit={true}
                        style={styles.titleText}>WELCOME</Text>
                </View>
                <View style={styles.formContainer}>
                    <LoginForm {...this.props}/>
                </View>

            </KeyboardAvoidingView>
        );
    }
}


// define your styles
const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        backgroundColor: '#FFF',
    },
    titleText: {
        fontSize: 40,
        justifyContent: 'center',
        alignItems: 'center',
        fontWeight: 'bold'
    },

    title: {
        color: "#FFF",
        marginTop: 10,
        width: 180,
        textAlign: 'center',
        opacity: 0.9
    },
    titleText: {
        flex: 1,
        fontSize: 40,
        fontWeight: 'bold',
        justifyContent: 'center',
        width: 200,
        alignItems: 'center'
    },
});

export default Login;
