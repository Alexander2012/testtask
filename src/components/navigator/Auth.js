import {AsyncStorage} from "react-native";

export const USER_KEY = "auth-key";

export const USER_NAME = "user_name";

export const onSaveName = (name) => AsyncStorage.setItem(USER_NAME, name);

export const onGetName = async () => await AsyncStorage.getItem(USER_NAME);

export const onRemoveName = () => AsyncStorage.removeItem(USER_NAME);

export const onSignIn = () => AsyncStorage.setItem(USER_KEY, "true");

export const onSignOut = () => AsyncStorage.removeItem(USER_KEY);

export const isSignedIn = () => {
    return new Promise((resolve, reject) => {
        AsyncStorage.getItem(USER_KEY)
            .then(res => {
                if (res !== null) {
                    resolve(true);
                } else {
                    resolve(false);
                }
            })
            .catch(err => reject(err));
    });
};

