import { StackNavigator, DrawerNavigator } from "react-navigation";
import Login from "../Login/Login";
import React from "react";
import MenuScreen from "../MainScreen/MenuScreen";
import StackOverFlow from "../MainScreen/StackOverflow";
import Logout from "../MainScreen/Logout";

export const SignedOut = StackNavigator({
    SignedOut: {
        screen: Login,
        navigationOptions: {
        }
    }
});

export const createRootNavigator = (signedIn = false) => {
    return StackNavigator(
        {
            SignedIn: {
                screen: Navigator,
            },
            SignedOut: {
                screen: Login
            }
        },
        {
            headerMode: "none",
            mode: "modal",
            initialRouteName: signedIn ? "SignedIn" : "SignedOut"
        }
    );
};


export const Navigator = DrawerNavigator({

    MenuScreen: {screen: MenuScreen},
    StackOverFlow: {screen: StackOverFlow},
    Logout: {screen: Logout},
});

